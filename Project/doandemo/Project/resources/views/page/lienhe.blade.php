@extends('master')
@section('content')
<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Liên Hệ</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="{{route('trang-chu')}}">Home</a> / <span>Contacts</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="beta-map">
		
		<div class="abs-fullwidth beta-map wow flipInX"><iframe src="https://www.google.com/maps/place/Ch%C3%B9a+M%E1%BB%99t+C%E1%BB%99t/@21.0358565,105.8336184,15z/data=!4m2!3m1!1s0x0:0xdbe7366fe9dfc2ae?sa=X&ved=2ahUKEwjCqKrC6P_hAhXXdd4KHXp9AdUQ_BIwDnoECB4QCA" ></iframe></div>
	</div>
	<div class="container">
		<div id="content" class="space-top-none">
			
			<div class="space50">&nbsp;</div>
			<div class="row">
				<div class="col-sm-8">
					<h2>Thông tin liên hệ</h2>
					<div class="space20">&nbsp;</div>
					<p>Vui lòng để lại thông tin nếu bạn cần trợ giúp, chúng tôi sẽ phản hồi bạn sớm nhất, trân trọng cảm ơn.</p>
					<div class="space20">&nbsp;</div>
					<form action="{{route('trang-chu')}}" method="get" class="contact-form">	
						<div class="form-block">
							<input name="your-name" type="text" placeholder="Tên của bạn (Bắt buộc)">
						</div>
						<div class="form-block">
							<input name="your-email" type="email" placeholder="Email của bạn ">
						</div>
						<div class="form-block">
							<input name="your-subject" type="text" placeholder="Vấn đề bạn muốn trợ giúp">
						</div>
						<div class="form-block">
							<textarea name="your-message" placeholder="Nội dung"></textarea>
						</div>
						<div class="form-block">
							<button type="submit" class="beta-btn primary">Gửi phản hồi<i class="fa fa-chevron-right"></i></button>
						</div>
					</form>
				</div>
				<div class="col-sm-4">
					<h2>Thông tin về cửa hàng</h2>
					<div class="space20">&nbsp;</div>

					<h6 class="contact-title">Địa chỉ</h6>
					<p>
						17A, Long Biên, Hà Nội,<br>
						Ngõ 555 <br>
						VietNam
					</p>
					<div class="space20">&nbsp;</div>
					<h6 class="contact-title">Chủ sở hữu</h6>
					<p>
						Nguyễn Công Hoàng Phong, <br>
						SĐT: +84 32548213465. <br>
						<a href="mailto:biz@betadesign.com">nchp@gmail.com</a>
					</p>
					<div class="space20">&nbsp;</div>
					<h6 class="contact-title">Nhân viên và tuyển dụng</h6>
					<p>
						Chúng tôi luôn mở cửa chào đón các bạn đến làm việc tại cửa hàng. <br>
						Liên hệ MR.Phong hoặc gửi mail đến địa chỉ. <br>
						<a href="hr@betadesign.com">store_work@gmail.com</a>
					</p>
				</div>
			</div>
		</div> <!-- #content -->
    </div> <!-- .container -->
@endsection