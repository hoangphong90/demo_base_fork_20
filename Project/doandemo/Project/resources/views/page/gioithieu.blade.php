@extends('master')
@section('content')
<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Giới thiệu</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="{{route('trang-chu')}}">Home</a> / <span>Giới thiệu</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="container">
		<div id="content">
			<div class="our-history">
				<h2 class="text-center wow fadeInDown">Biên Niên Sử HP Store</h2>
				<div class="space35">&nbsp;</div>

				<div class="history-slider">
					<div class="history-navigation">
						<a data-slide-index="0" href="blog_with_2sidebars_type_e.html" class="circle"><span class="auto-center">2017</span></a>
						<a data-slide-index="1" href="blog_with_2sidebars_type_e.html" class="circle"><span class="auto-center">2018</span></a>
						<a data-slide-index="2" href="blog_with_2sidebars_type_e.html" class="circle"><span class="auto-center">2019</span></a>
					</div>

					<div class="history-slides">
						<div> 
							<div class="row">
							<div class="col-sm-5">
								<img src="assets/dest/images/history.jpg" alt="">
							</div>
							<div class="col-sm-7">
								<h5 class="other-title">Ngày Thành Lập</h5>
								<p>
									Ngày 17 tháng 4 năm 2017<br />
									Số 17A, Giang Biên, Long Biên, Hà Nội<br />
									VietNam
								</p>
								<div class="space20">&nbsp;</div>
								<p>Cửa hàng được thiết kế và xây dựng theo xu hướng và nhu cầu đọc sách của người Việt.</p>
							</div>
							</div> 
						</div>
						<div> 
							<div class="row">
							<div class="col-sm-5">
								<img src="assets/dest/images/history.jpg" alt="">
							</div>
							<div class="col-sm-7">
								<h5 class="other-title">Cửa hàng chính thức đi vào hoạt động</h5>
								<p>
									Giờ mở cửa: 8 AM - 10 PM<br />
									Từ thứ 2 đến thứ 7<br />
									Đóng cửa vào ngày lễ, chủ nhật
								</p>
								<div class="space20">&nbsp;</div>
								<p>Hiệu sách HP chính thức đi vào hoạt động với nhiều ưu đãi mới, dịch vụ chăm sóc khách hàng chu đáo và cửa hàng online 24/24.</p>
							</div>
							</div> 
						</div>
						<div> 
							<div class="row">
							<div class="col-sm-5">
								<img src="assets/dest/images/history.jpg" alt="">
							</div>
							<div class="col-sm-7">
								<h5 class="other-title">Mở rộng chi nhánh, quy mô cửa hàng</h5>
								<p>
									20 Cửa hàng khắp Hà Nội,<br />
									Cổng thông tin trực tuyến phục vụ 24/24<br />
									Tư vấn hoàn toàn miễn phí
								</p>
								<div class="space20">&nbsp;</div>
								<p>Sau một năm thành lập, nhận được sự giúp đỡ và ủng hộ của khách hàng, Hiệu sách quyết định mở rộng thêm các chi nhánh mới phục vụ tối đa nhu cầu và thị trường người dùng.</p>
							</div>
							</div> 
						</div>						
					</div>
				</div>
			</div>

			<div class="space50">&nbsp;</div>
			<hr />
			<div class="space50">&nbsp;</div>
			<h2 class="text-center wow fadeInDown">Mang đến trải nghiệm tốt nhất cho người tiêu dùng</h2>
			<div class="space20">&nbsp;</div>
			<p class="text-center wow fadeInLeft">Với trách nhiệm của mình, chúng tôi luôn sẵn sàng mang đến cho người dùng những cập nhật mới nhất, những chính sách chăm sóc và ưu đãi tốt nhất. </p>
			<div class="space35">&nbsp;</div>

			<div class="row">
				<div class="col-sm-2 col-sm-push-2">
					<div class="beta-counter">
						<p class="beta-counter-icon"><i class="fa fa-user"></i></p>
						<p class="beta-counter-value timer numbers" data-to="19855" data-speed="2000">1704</p>
						<p class="beta-counter-title">Người dùng</p>
					</div>
				</div>

				<div class="col-sm-2 col-sm-push-2">
					<div class="beta-counter">
						<p class="beta-counter-icon"><i class="fa fa-picture-o"></i></p>
						<p class="beta-counter-value timer numbers" data-to="3568" data-speed="2000">2000</p>
						<p class="beta-counter-title">Đầu sách bán ra</p>
					</div>
				</div>

				<div class="col-sm-2 col-sm-push-2">
					<div class="beta-counter">
						<p class="beta-counter-icon"><i class="fa fa-clock-o"></i></p>
						<p class="beta-counter-value timer numbers" data-to="258934" data-speed="2000">9120</p>
						<p class="beta-counter-title">Thời gian tư vấn</p>
					</div>
				</div>

				<div class="col-sm-2 col-sm-push-2">
					<div class="beta-counter">
						<p class="beta-counter-icon"><i class="fa fa-pencil"></i></p>
						<p class="beta-counter-value timer numbers" data-to="150" data-speed="2000">150</p>
						<p class="beta-counter-title">Bài đánh giá</p>
					</div>
				</div>
			</div> <!-- .beta-counter block end -->

			
		</div> <!-- #content -->
    </div> <!-- .container -->
@endsection